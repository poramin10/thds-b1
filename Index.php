<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>กรมอนามัย
    </title>

    <?php include 'assets/include/inc-head.php'; ?>
</head>

<body class="sidebar-double-icon  ">
    <div id="loading">
        <div id="loading-center">
        </div>
    </div>

    <div class="wrapper">
        <?php include 'assets/include/inc-menuleft.php'; ?>
        <?php include 'assets/include/inc-header.php'; ?>
        <div class="content-page">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <h3><b>ข่าวประชาสัมพันธ์</b></h3>
                    </div>
                    <?php for ($i = 0; $i < 8; $i++) { ?>
                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <div class="card">
                                <img src="assets/images/page-img/07.jpg" class="card-img-top" alt="#">
                                <div class="card-body">
                                    <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem ipsa assumenda explicabo unde alias voluptates provident enim dolore labore </p>
                                    <a href="#" class="card-link text-primary">อ่านเพิ่มเติม</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>


            </div>
        </div>
    </div>

    <?php include 'assets/include/inc-footer.php' ?>
    <?php include 'assets/include/inc-script.php' ?>
</body>

</html>