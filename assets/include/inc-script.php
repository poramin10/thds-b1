 <!-- Backend Bundle JavaScript -->
 <script src="assets/js/backend-bundle.min.js"></script>

<!-- Flextree Javascript-->
<script src="assets/js/flex-tree.min.js"></script>
<script src="assets/js/tree.js"></script>

<!-- Table Treeview JavaScript -->
<script src="assets/js/table-treeview.js"></script>

<!-- Masonary Gallery Javascript -->
<script src="assets/js/masonry.pkgd.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>

<!-- Mapbox Javascript -->
<script src="assets/js/mapbox-gl.js"></script>
<script src="assets/js/mapbox.js"></script>

<!-- Fullcalender Javascript -->
<script src='assets/vendor/fullcalendar/core/main.js'></script>
<script src='assets/vendor/fullcalendar/daygrid/main.js'></script>
<script src='assets/vendor/fullcalendar/timegrid/main.js'></script>
<script src='assets/vendor/fullcalendar/list/main.js'></script>

<!-- SweetAlert JavaScript -->
<script src="assets/js/sweetalert.js"></script>

<!-- Vectoe Map JavaScript -->
<script src="assets/js/vector-map-custom.js"></script>

<!-- Chart Custom JavaScript -->
<script src="assets/js/customizer.js"></script>

<!-- Chart Custom JavaScript -->
<script src="assets/js/chart-custom.js"></script>

<!-- slider JavaScript -->
<script src="assets/js/slider.js"></script>

<!-- app JavaScript -->
<script src="assets/js/app.js"></script>