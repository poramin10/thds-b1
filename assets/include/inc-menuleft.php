<div class="iq-sidebar sidebar-double-icon">
            <div class="iq-sidebar-logo d-flex align-items-center justify-content-between">
                <a href="index.php" class="header-logo mx-auto">
                    <img src="assets/images/1-main/logo-intro.png" style="width: auto; height: auto" class="img-fluid rounded-normal" alt="logo">
                </a>
                <div class="iq-menu-bt-sidebar">
                    <i class="las la-bars wrapper-menu"></i>
                </div>
            </div>
            <div id="sidebar-scrollbar" class="data-scrollbar" data-scroll="1">
                <div class="d-block user-info mb-3">
                    <div class="text-center mb-4">
                        <div class="d-inline-block doc-img user-image mb-3 rounded position-relative">
                            <img src="assets/images/user/1.jpg" class="img-fluid d-inline-block avatar-110 rounded" alt="logo">
                        </div>
                        <h4>Cliff Hanger</h4>
                        <p>cliff@admin.com</p>
                    </div>
                </div>
                <div class="double-icon-height">
                    <nav class="iq-sidebar-menu">
                        <ul id="iq-sidebar-toggle" class="iq-menu">
                            <li>
                                <a href="index.php" class="collapsed" data-toggle="collapse" aria-expanded="false"><i class="las la-newspaper iq-arrow-left"></i>&nbsp;<span>ข่าวประชาสัมพันธ์</span></a>
                            </li>
                        </ul>
                        <ul id="iq-sidebar-toggle" class="iq-menu">
                            <li>
                                <a href="" class="collapsed" data-toggle="collapse" aria-expanded="false"><i class="las la-address-card iq-arrow-left"></i>&nbsp;<span>บัตรของฉัน</span></a>
                            </li>
                        </ul>
                        <ul id="iq-sidebar-toggle" class="iq-menu">
                            <li>
                                <a href="" class="collapsed" data-toggle="collapse" aria-expanded="false"><i class="las la-file-alt iq-arrow-left"></i>&nbsp;<span>ลงทะเบียนขอบัตร</span></a>
                            </li>
                        </ul>
                        <ul id="iq-sidebar-toggle" class="iq-menu">
                            <li>
                                <a href="" class="collapsed" data-toggle="collapse" aria-expanded="false"><i class="las la-list iq-arrow-left"></i>&nbsp;<span>ลงทะเบียนอื่นๆ</span></a>
                            </li>
                        </ul>
                        <ul id="iq-sidebar-toggle" class="iq-menu">
                            <li>
                                <a href="" class="collapsed" data-toggle="collapse" aria-expanded="false"><i class="las la-chalkboard-teacher iq-arrow-left"></i>&nbsp;<span>ลงทะเบียน<br>หน่วยจัดการอบรม</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="sidebar-bottom-menu">
                    <nav class="iq-sidebar-menu">
                        <ul class="iq-menu-bottom list-inline">
                            <li><a href="#"><i class="las la-envelope-open iq-arrow-left"></i><span>แจ้งร้องเรียน </span><!-- <span class="float-right badge badge-warning rounded-circle menu-icon-right">18</span> --></a></li>
                        </ul>
                        <div class="text-center mt-3">
                            <a href="login.php" class="btn btn-primary"><i class="las la-sign-out-alt"></i>ออกจากระบบ</a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>