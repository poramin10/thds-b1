<div class="iq-top-navbar">
    <div class="iq-navbar-custom d-flex align-items-center justify-content-between">
        <div class="iq-navbar-logo d-flex align-items-center justify-content-between">
            <i class="ri-menu-line wrapper-menu"></i>
            <a href="index.html" class="header-logo">
                <img src="assets/images/1-main/logo-intro.png" class="img-fluid rounded-normal" alt="logo">
            </a>
        </div>

        <div class="iq-menu-horizontal">
            <ul class="navbar-nav mr-auto navbar-list align-items-center">
                <li class="nav-item iq-full-screen"><b>ระบบการขอมีบัตรประจำตัวและ<br>ประกาศนียบัตรภายใต้ภารกิจของกรมอนามัย</b></li>
            </ul>
        </div>

        <nav class="navbar navbar-expand-lg school-navbar navbar-light p-0">

            <div class="change-mode">
                <div class="custom-control custom-switch custom-switch-icon custom-control-inline">
                    <div class="custom-switch-inner">
                        <p class="mb-0"> </p>
                        <input type="checkbox" class="custom-control-input" id="dark-mode" data-active="true">
                        <label class="custom-control-label" for="dark-mode" data-mode="toggle">
                            <span class="switch-icon-left"><i class="a-left"></i></span>
                            <span class="switch-icon-right"><i class="a-right"></i></span>
                        </label>
                    </div>
                </div>
            </div>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-label="Toggle navigation">
                <i class="ri-menu-3-line"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto navbar-list align-items-center">
                    <li class="nav-item iq-full-screen"><a href="#" class="" id="btnFullscreen"><i class="ri-fullscreen-line"></i></a></li>

                    <li class="nav-item nav-icon dropdown">
                        <a href="#" class="search-toggle dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="assets/images/user/1.jpg" class="img-fluid rounded" alt="user">
                        </a>
                        <div class="iq-sub-dropdown dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <div class="card shadow-none m-0">
                                <div class="card-body p-0 ">
                                    <div class="cust-title p-3">
                                        <h5 class="mb-0">กิตติปริภัทร ย่ำแท่น</h5>
                                    </div>
                                    <div class="p-3">
                                        <a href="#" class="iq-sub-card">
                                            <div class="media align-items-center">

                                                <div class="media-body ml-3">
                                                    <h6 class="mb-0">ข้อมูลส่วนตัว </h6>
                                                </div>
                                            </div>
                                        </a>

                                    </div>
                                    <a class="right-ic btn btn-primary btn-block position-relative p-2" href="#" role="button">
                                        <div class="dd-icon"><i class="las la-arrow-right mr-0"></i></div>
                                        ออกจากระบบ
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>