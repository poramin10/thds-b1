<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/favicon.ico" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Sarabun:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">
<link rel="stylesheet" href="assets/css/backend.min.css">
<link rel="stylesheet" href="assets/css/custom.css">
<link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="assets/vendor/line-awesome/dist/line-awesome/css/line-awesome.min.css">
<link rel="stylesheet" href="assets/vendor/remixicon/fonts/remixicon.css">
<link rel="stylesheet" href="assets/vendor/@icon/dripicons/dripicons.css">

<link rel='stylesheet' href='assets/vendor/fullcalendar/core/main.css' />
<link rel='stylesheet' href='assets/vendor/fullcalendar/daygrid/main.css' />
<link rel='stylesheet' href='assets/vendor/fullcalendar/timegrid/main.css' />
<link rel='stylesheet' href='assets/vendor/fullcalendar/list/main.css' />
<link rel="stylesheet" href="assets/vendor/mapbox/mapbox-gl.css">