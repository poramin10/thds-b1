<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>กรมอนามัย
    </title>

    <?php include 'assets/include/inc-head.php'; ?>
</head>

<body class="sidebar-double-icon intro-page">
    <div id="loading">
        <div id="loading-center">
        </div>
    </div>
    <div class="wrapper">
        <section class="intro-content">
            <img src="assets/images/1-main/bg-building.png" class="intro-img" alt="">

            <div class="row align-items-center justify-content-center">
                <div class="col-12">
                    <div class="text-center logo-intro">
                        <img src="assets/images/1-main/logo-intro.png" class="" alt="">
                        <h3 class="title-system">ระบบการขอมีบัตรประจำตัวและ <br>
                            ประกาศนียบัตรภายใต้ภารกิจของกรมอนามัย
                        </h3>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                   <a href="register.php">
                        <div class="card">
                            <div class="card-body text-primary">
                                <img src="assets/images/1-main/btn-register.png" class="mt-4" alt="" width="60">
                                <hr class="w-10">
                                <h4 class="card-title text-primary">ลงทะเบียนบัญชีผู้ใช้</h4>
                            </div>
                        </div>
                   </a>
                </div>
                <div class=" col-md-2 col-sm-6">
                    <a href="login.php">
                        <div class="card">
                            <div class="card-body text-primary">
                                <img src="assets/images/1-main/btn-login.png" class="mt-4" alt="" width="60">
                                <hr class="w-10">
                                <h4 class="card-title text-primary">เข้าสู่ระบบ</h4>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-2 col-sm-6">
                   <a href="healthpoint.php">
                        <div class="card">
                            <div class="card-body text-primary">
                                <img src="assets/images/1-main/btn-point.png" class="mt-4" alt="" width="60">
                                <hr class="w-10">
                                <h4 class="card-title text-primary">ตรวจสอบคะแนน </h4>
                                <h4 class="card-title text-primary">Helth Point </h4>
                            </div>
                        </div>
                   </a>
                </div>

            </div>
        </section>
    </div>
    <?php include 'assets/include/inc-script.php' ?>
</body>

</html>