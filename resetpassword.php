<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>กรมอนามัย
    </title>

    <?php include 'assets/include/inc-head.php'; ?>
</head>

<body class="sidebar-double-icon intro-page">
    <div id="loading">
        <div id="loading-center">
        </div>
    </div>
    <div class="wrapper">
        <section class="login-content">
            <img src="assets/images/1-main/bg-building.png" class="intro-img" alt="">
            <div class="container mt-4">
                <div class="text-center">
                    <img src="assets/images/1-main/logo-intro.png" class="" alt="">
                    <h3 class="title-system"> ระบบการขอมีบัตรประจำตัวและ <br>
                        ประกาศนียบัตรภายใต้ภารกิจของกรมอนามัย
                    </h3>
                </div>
                <div class="row mt-5 justify-content-center">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mt-3">รีเซ็ตรหัสผ่าน</h4>
                                <form>
                                    <div class="row">
                                        <div class="col">
                                            <input type="text" class="form-control mt-3" placeholder="ชื่อผู้ใช้งาน หรืออีเมล">
                                        </div>
                                    </div>
                                </form>
                                <a href="#" class="btn btn-primary mt-3" style="float: right">ยืนยัน</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include 'assets/include/inc-script.php' ?>
</body>

</html>