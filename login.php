<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>กรมอนามัย
    </title>

    <?php include 'assets/include/inc-head.php'; ?>
</head>

<body class="sidebar-double-icon intro-page">
    <div id="loading">
        <div id="loading-center">
        </div>
    </div>
    <div class="wrapper">

        <section class="login-content">
            <img src="assets/images/1-main/bg-building.png" class="intro-img" alt="">
            <div class="container">
                <div class="row align-items-center justify-content-center h-100">
                    <div class="col-12">
                        <div class="text-center logo-intro">
                            <img src="assets/images/1-main/logo-intro.png" class="" alt="">
                            <h3 class="title-system">ระบบการขอมีบัตรประจำตัวและ <br>
                                ประกาศนียบัตรภายใต้ภารกิจของกรมอนามัย
                            </h3>
                        </div>
                    </div>
                    <div class="col-lg-4 ">
                        <div class="card">
                            <div class="card-header justify-content-between">
                                <div class="header-title text-cnter">
                                    <h4 class="card-title text-center">เข้าสู่ระบบ</h4>
                                </div>
                            </div>
                            <div class="card-body">
                                <form>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="email" placeholder=" ">
                                                <label>ชื่อผู้ใช้งาน</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="floating-label form-group">
                                                <input class="floating-input form-control" type="password" placeholder=" ">
                                                <label>รหัสผ่าน</label>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <a href="resetpassword.php" class="text-primary text-left">
                                                ลืมรหัสผ่าน
                                            </a>
                                        </div>
                                    </div>

                                    <!-- <button type="submit" class="btn btn-primary btn-lg btn-block">เข้าสู่ระบบ</button> -->
                                    <a href="index.php" class="btn btn-primary btn-lg btn-block mt-4">เข้าสู่ระบบ</a>
                                    <div class="mt-4 mb-2 text-center">
                                        ยังไม่มีบัญชีผู้ใช้ <a href="register.php" class="text-primary">ลงทะเบียน</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include 'assets/include/inc-script.php' ?>
</body>

</html>