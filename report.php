<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>กรมอนามัย</title>

    <?php include 'assets/include/inc-head.php'; ?>
</head>

<body class="sidebar-double-icon intro-page">
    <div id="loading">
        <div id="loading-center">
        </div>
    </div>
    <div class="wrapper">
        <section class="login-content" style="height:auto">
            <img src="assets/images/1-main/bg-building.png" class="intro-img" alt="">
            <div class="container ">
                <div class="mt-1 text-center">
                    <img src="assets/images/1-main/logo-intro.png" class="" alt="">
                    <br><br>
                    <h3>ระบบการขอมีบัตรประจำตัวและ
                        ประกาศนียบัตรภายใต้ภารกิจของกรมอนามัย</h3>
                </div>

                <div class="row align-items-center justify-content-center mt-4">
                    <div class="col-sm-12 col-lg-9">
                        <div class="card">
                            <div class="card-header justify-content-between">
                                <div class="header-title">
                                    <h4 class="card-title text-center">แจ้งเรื่องร้องเรียน</h4>
                                </div>
                            </div>


                            <div class="card-body">
                                <div class="form-row">
                                    <label class="col-lg-6">
                                        <input type="text" class="form-control" placeholder="เลขบัตรประชาชน">
                                    </label>
                                </div>

                                <div class="form-row">
                                    <label class="col-lg-6">
                                        <input type="text" class="form-control" placeholder="ชื่อ">
                                    </label>
                                    <label class="col-lg-6">
                                        <input type="text" class="form-control" placeholder="นามสกุล">
                                    </label>
                                </div>

                                <div class="form-row">
                                    <label class="col-lg-6">
                                        <input type="text" class="form-control" placeholder="เบอร์โทรศัพท์">
                                    </label>
                                    <label class="col-lg-6">
                                        <input type="email" class="form-control" placeholder="อีเมล">
                                    </label>
                                </div>


                                <div class="form-row">
                                    <label class="col-lg-12">
                                        <select class="form-control">
                                            <option selected="">หน่วยงานร้องเรียน</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="form-row">
                                    <label class="col-lg-12">
                                        <textarea class="textarea form-control" rows="2" placeholder="รายละเอียดการร้องเรียน"></textarea>
                                    </label>
                                </div>

                                <div class="form-row">
                                    <label class="col-lg-12">
                                        <input type="text" class="form-control" placeholder="สถานที่ต้องการร้องเรียน">
                                    </label>
                                </div>

                                <div class="form-row">
                                    <label class="col-lg-4">
                                        <select class="form-control">
                                            <option selected="">จังหวัด</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </label>
                                    <label class="col-lg-4">
                                        <select class="form-control">
                                            <option selected="">อำเภอ</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </label>
                                    <label class="col-lg-4">
                                        <select class="form-control">
                                            <option selected="">ตำบล</option>
                                            <option value="1">#</option>
                                            <option value="2">#</option>
                                            <option value="3">#</option>
                                        </select>
                                    </label>
                                </div>

                                <label class="mt-2">อัปโหลดไฟล (เอกสาร,คลิปวีดิโอ,อื่นๆ)</label>
                                <div class="form-row">
                                    <label class="col-lg-6">
                                        <input type="file" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">เลือกไฟล์</label>
                                    </label>
                                    <label class="col-lg-6">
                                    </label>
                                </div>
                            </div>
                        </div>

                        <center><button type="button" class="btn btn-primary mt-2 mb-5">ยื่นเรื่อง</button></center>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include 'assets/include/inc-script.php' ?>
</body>

</html>